<?php 

stream_context_set_default([
	'tsl'=> [
		'verify_peer' => false,
		'verify_peer_name' => false,
	]
]);
$fp = fsockopen("www.google.com", 80, $errno, $errstr, 10); // works fine
if(!$fp)
    echo "www.google.com - $errstr ($errno)<br>\n";
else
    echo "www.google.com - ok<br>\n";
 
$fp = fsockopen("tls://smtp.gmail.com", 465, $errno, $errstr, 10); // Does not work
if(!$fp)
    echo "smtp.gmail.com 465 - $errstr ($errno)<br>\n";
else
    echo "smtp.gmail.com 465 - ok<br>\n";
 
$fp = fsockopen("tls://smtp.gmail.com", 587, $errno, $errstr, 10); // Does not work
if(!$fp)
    echo "smtp.gmail.com 587 - $errstr ($errno)<br>\n";
else
    echo "smtp.gmail.com 587 - ok<br>\n";
//echo "<br />".phpinfo(); 
/*echo "<pre>";
echo var_dump(openssl_get_cert_locations());*/
